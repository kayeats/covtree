def print_covtree(CT):#from Karen's code
  #print("Elements of covtree with witnesses of size up to " + str(n-1))
  for i in range(len(CT)):# i indexes the level
    print("Elements involving posets of size " + str(i))
    for j in CT[i]: # j is a node at level i
      print("elt with " + str(len(j)) + " poset(s) of size " + str(i) + ": {")
      for k in j: # k is an i-order in j
        print("["+str(k.list())+","+str(k.cover_relations())+"]")
      print("}")

def build_covtree_v6(n):#fastest covtree generator which only assumes proven properties (i.e. doesn't assume the expected witness size conjecture).
    CT = [[] for i in range(n)]#CT is a list of lists, each list in CT is a covtree level
    for witness_size in range(n):
        P = Posets(witness_size)
        i = 0
        for witness in P: #start the procedure of producing nodes from witnesses of certain size
            if (i%100 == 0):
               print(i)
            i += 1
            #print("New witness of size", witness_size)
            counter=1#just a checker, counts the number of new nodes witnessed by "witness"
            #print(".", end="", flush=True) 
            level_sets = witness.order_ideals_lattice().level_sets() 
            nodes = []#list of nodes for which "witness" is a witness
            for size in range(len(level_sets)):
                nodes.append({witness.subposet(elts).canonical_label() for elts in level_sets[size]})   
            CT[witness_size].append(nodes[witness_size])
            for size in reversed(range(len(nodes)-1)):
                if len(nodes[size])==1:
                 #   print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "due to cardinality=1. Witness contributed", counter, "nodes.")
                    break #move on to next witness
                elif len(nodes[size])==2 and size+1<witness_size:
                  #  print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "due to cardinality=2. Witness contributed", counter, "nodes.")
                    break #move on to next witness
                else:
                    if nodes[size] not in CT[size]:
                        CT[size].append(nodes[size])
                        counter+=1 
                    else:
                       # print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "since duplicate found. Witness contributed", counter, "nodes.")
                       break #move on to next witness
               
        #print("")
    return CT

def build_covtree_v8(n):#v8 is same as v6, except it assumes that the expected witness size conjectures is correct so abandones witnesses whenever n+k-1<witness_size for any k. I compared its output with that of v6 for n=7 (provides evidence for the conjecture). v8 was 9s faster than v6 on n=8 (102s).
    CT = [[] for i in range(n)]#CT is a list of lists, each list in CT is a covtree level
    for witness_size in range(n):
        kounter=0#just a checker. counts number of dropouts due to cardinality=1 at first peel for each witness_size,
        P = Posets(witness_size)
        for witness in P: #start the procedure of producing nodes from witnesses of certain size
            #print("New witness of size", witness_size)
            counter=1#just a checker, counts the number of new nodes witnessed by "witness"
            #print(".", end="", flush=True)#end makes the dots corresponding to the same witness size to print on one line, flush makes the dots come one by one as the process goes on, rather than all together at the end 
            level_sets = witness.order_ideals_lattice().level_sets() #.order_ideals_lattice() gets labelled n-stems for all n of witness and orders them into a lattice by inclusion
                                                                     #level_sets() returns a list l with l[0]=list of minimal elements, l[1]=list of elements at level 1 etc
                                                                     #altogether, level_sets is a list whose ith element is the set of labelled i-stems of witness
            nodes = []#list of nodes for which "witness" is a witness
            for size in range(len(level_sets)):
                nodes.append({witness.subposet(elts).canonical_label() for elts in level_sets[size]})   
            CT[witness_size].append(nodes[witness_size])
            #print("added to covtree successfully")
            #if len(nodes)-1==0:#just a checker for the empty poset
             #   print("witness contributed", counter, "node.")               
            for size in reversed(range(len(nodes)-1)):
                if len(nodes[size])+size-1 < witness_size:
                 #   print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "due to witness size exceeding the expected witness size. Witness contributed", counter, "nodes.")#just a checker. the first peel checks whether nodes[len(nodes)-1] contains more than 1 unlablled stem etc. the original build_covtree does witness_size peels for every witness
                    break #move on to next witness
                else:
                    if nodes[size] not in CT[size]:
                        CT[size].append(nodes[size])
                        #print("true!")
                        counter+=1 
                    else:
                       # print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "since duplicate found. Witness contributed", counter, "nodes.")#just a checker. the first peel checks whether nodes[len(nodes)-1] contains more than 1 unlablled stem etc. the original build_covtree does witness_size peels for every witness
                       break #move on to next witness
               
        #print("")
    return CT


def build_covtree_v7(n):#same as v6, but with additional output list "data" which stores information about the difference between smallest witness size and expected witness size. Slightly slower than v6 (4s slower on n=8).
    CT = [[] for i in range(n)]#CT is a list of lists, each list in CT is a covtree level
    data=[]#list of lists [level, cardinality, smallest witness size - expected witness size] for every node
    for witness_size in range(n):
        kounter=0#just a checker. counts number of dropouts due to cardinality=1 at first peel for each witness_size,
        P = Posets(witness_size)
        for witness in P: #start the procedure of producing nodes from witnesses of certain size
            #print("New witness of size", witness_size)
            counter=1#just a checker, counts the number of new nodes witnessed by "witness"
            #print(".", end="", flush=True)#end makes the dots corresponding to the same witness size to print on one line, flush makes the dots come one by one as the process goes on, rather than all together at the end 
            level_sets = witness.order_ideals_lattice().level_sets() #.order_ideals_lattice() gets labelled n-stems for all n of witness and orders them into a lattice by inclusion
                                                                     #level_sets() returns a list l with l[0]=list of minimal elements, l[1]=list of elements at level 1 etc
                                                                     #altogether, level_sets is a list whose ith element is the set of labelled i-stems of witness
            nodes = []#list of nodes for which "witness" is a witness
            for size in range(len(level_sets)):
                nodes.append({witness.subposet(elts).canonical_label() for elts in level_sets[size]})   
            CT[witness_size].append(nodes[witness_size])
            data.append([witness_size, 1, 0])
            #print("added to covtree successfully")
           # if len(nodes)-1==0:#just a checker for the empty poset
               # print("witness contributed", counter, "node.")               
            for size in reversed(range(len(nodes)-1)):
                if len(nodes[size])==1:
                   # print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "due to cardinality=1. Witness contributed", counter, "nodes.")#just a checker. the first peel checks whether nodes[len(nodes)-1] contains more than 1 unlablled stem etc. the original build_covtree does witness_size peels for every witness
                    break #move on to next witness
                elif len(nodes[size])==2 and size+1<witness_size:
                   # print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "due to cardinality=2. Witness contributed", counter, "nodes.")#just a checker. the first peel checks whether nodes[len(nodes)-1] contains more than 1 unlablled stem etc. the original build_covtree does witness_size peels for every witness
                    break #move on to next witness
                else:
                    if nodes[size] not in CT[size]:
                        CT[size].append(nodes[size])
                        data.append([size, len(nodes[size]), witness_size-size-len(nodes[size])+1])
                        #print("true!")
                        counter+=1 
                    else:
                    #    print("Aborted at peel number", len(nodes)-(size+1), " out of", witness_size, "since duplicate found. Witness contributed", counter, "nodes.")#just a checker. the first peel checks whether nodes[len(nodes)-1] contains more than 1 unlablled stem etc. the original build_covtree does witness_size peels for every witness
                        break #move on to next witness
               
        #print("")
    return CT, data

def expected_witness_size_checker(covtree_data):#takes in "data" from build_covtree_v7, returns False if counter example to expected witness size conjecture is found in the data
    for i in covtree_data:
        #print(i)
        if i[2]>0:
            return False
    return True

#example:
#covtree, covtree_data=build_covtree_v6(8)
#expected_witness_size_checker(covtree_data)

def partition_checker(node, level):#checks if node can be partitioned into 2 nodes in level. Example of node which cannot be partitioned: [[3, 2, 1, 0],[[3, 2], [3, 1], [2, 0]]]
#[[2, 3, 1, 0],[[2, 3], [3, 1]]]
#[[3, 2, 1, 0],[[3, 2], [3, 1]]]
#Only found nodes of card 3 which cannot be partitioned, but this maybe due to k+n-1<witness size constraint.
    #SetPartitions(node,2).list()
    N=[]#node as list
    #print(N)
    for order in node:
      #  print(order)
        N.append(order)
     #   print(N)
    for i in SetPartitions({k for k in range(len(node))},2):#i is a partition of node into 2 non-empty sets
        print("partition=",i)
        counter=0
        for j in i:#j is a frozen set, set(j) is a set
            print(set(j))
            for r in set(j):
                S={N[r] for r in set(j)}
            print("S=",S)
            if S not in level:
                print("S not in level")
                break
            else:
                print("S found in level")
                counter+=1
                if counter==2:
                   # print("True")
                    return True 
    
    for elt in node:
    	print("["+str(elt.list())+","+str(elt.cover_relations())+"]")
    return False

CT=build_covtree_v6(10)
print_covtree(CT)
import sys
original_stdout = sys.stdout # Save a reference to the original standard output
with open('covtree9.txt', 'w') as f:
  sys.stdout = f # Change the standard output to the file we created.
  print_covtree(CT)
  sys.stdout = original_stdout # Reset the standard output to its original value



#for level in CT:
#    print("level=",CT.index(level))
#    for node in level:
#        if len(node)==3:
#            if len(node)+CT.index(level)-1<=6:
#                print("new node")
#                partition_checker(node, level)



#timing the alternatives:
#import time
#start_time = time.time()
#build_covtree_v2(7) 
#print("--- %s seconds ---" % (time.time() - start_time))
