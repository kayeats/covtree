# how large the witnesses we will consider (largest considered is size n-1)
n = 9
k = 3
l = 5

# are two sets of unlablled posets the same in that their elts are isomorphic
def iso_sets(S1, S2):
  if (len(S1) == 0) or (len(S2) == 0):
    return len(S1)==len(S2)
  for i in S1:
    for j in S2:
      if i.is_isomorphic(j):
        if iso_sets(S1.difference(set([i])), S2.difference(set([j]))):
          return True
  return False


# append if distinct, for the case of sets of unlabelled posets
# L a list of sets of posets, S a set of posets
def append_distinct(L, S, witness):
  found = False
  for [elt, witness2] in L:
    if iso_sets(elt, S):
      found = True
  if not found:
    L.append([S, witness])

#print this behemoth
def print_covtree_with_witnesses(CT):
  #print("Elements of covtree with witnesses of size up to " + str(n-1))
  for i in range(len(CT)):
    print("Elements involving posets of size " + str(i))
    for [j,witness] in CT[i]:
      print("elt with " + str(len(j)) + " poset(s) of size " + str(i) + ": {")
      for k in j:
        print("["+str(k.list())+","+str(k.cover_relations())+"]")
      print("}")
      print("witness: "+str(witness.list())+","+str(witness.cover_relations()))

#or without witnesses
def print_covtree(CT):
  #print("Elements of covtree with witnesses of size up to " + str(n-1))
  for i in range(len(CT)):
    print("Elements involving posets of size " + str(i))
    for j in CT[i]:
      print("elt with " + str(len(j)) + " poset(s) of size " + str(i) + ": {")
      for k in j:
        print("["+str(k.list())+","+str(k.cover_relations())+"]")
      print("}")


# build all elements of covtree with witnesses up to size n - 1
# for now just collect them by the sizes of their elements, not building
# the poset structure of covtree itself.
def build_covtree(n):
  CT = [[] for i in range(n)]
  for witness_size in range(n):
    P = Posets(witness_size)
    for witness in P:
      print(".", end="", flush=True)
      level_sets = witness.order_ideals_lattice().level_sets()
      for size in range(len(level_sets)):
        append_distinct(CT[size], {witness.subposet(elts).canonical_label() for elts in level_sets[size]}, witness)
    print("")
  return CT


# build elements of covtree of cardinality k, level l and
# with witnesses up to size n - 1
def build_covtree_special(k,l,n):
  CT = [[] for i in range(n)]
  for witness_size in range(n):
    P = Posets(witness_size)
    for witness in P:
      print(".", end="", flush=True)
      level_sets = witness.order_ideals_lattice().level_sets()
      if l < len(level_sets):
        S = {witness.subposet(elts).canonical_label() for elts in level_sets[l]}
        if len(S) == k:
          append_distinct(CT[l], S, witness)
    print("")
  return CT


# make it simpler by just getting the pairs
# (which we know are covered by an element of size 1 larger)
def build_pairs_in_covtree(n):
  CT_piece = [[] for i in range(n)]
  for witness_size in range(n):
    P = Posets(witness_size)
    for witness in P:
      print(".", end="", flush=True)
      maxima = witness.maximal_elements()
      list_of_L = []
      for m in maxima:
        L = witness.list()     
        L.remove(m)
        list_of_L.append(L)
      elt = {witness.subposet(L).canonical_label() for L in list_of_L}
      if len(elt) == 2:
        append_distinct(CT_piece[witness_size-1], elt, witness)
    print("")
  return CT_piece

# build potential elements in the sense that there is connectivity in the
# graph by common covers.
# go up to posets on n-1 elements
# go up to sets of k posets
def build_potential_elements(n, k):
  PCT = [[] for i in range(n)]
  for rank in range(n):
    P = Posets(rank)
    G = Graph()
    G.add_vertices(range(len(P)))

    #build a dictionary of the elements covered by each poset in P
    coverD = {n: set() for n in G.vertices()}
    for v in G.vertices():
      list_of_covers = []
      maxima = P[v].maximal_elements()
      for m in maxima:
        L = P[v].list()     
        L.remove(m)
        list_of_covers.append(P[v].subposet(L).canonical_label())
      coverD[v] = set(list_of_covers)

    for v in range(len(P)):
      for w in range(v):
        if len(coverD[v].intersection(coverD[w]))>0:
          G.add_edge(v,w)
    print("made the graph for posets of size " + str(rank))
    for elt in G.connected_subgraph_iterator(k):
      append_distinct(PCT[rank], {P[v] for v in elt.vertices()})
    print("found the connected subgraphs for posets of size " + str(rank))
  return PCT 



##for building all of covtree up to a certain cover size
#CT = build_covtree(n)

##for building just elements with two posets in them
#CT = build_pairs_in_covtree(n)

##for building elements with level k and cardinality l
CT = build_covtree_special(k,l,n)

##for building potential elements of covtree
#CT = build_potential_elements(n, k)

print("Elements of covtree of cardinality "+str(k)+ " and level " + str(l) + " and witnesses up to "+str(n))
#print("Elements of covtree with witnesses of size up to "+str(n))
#print("Elements of covtree with two posets and witnesses of size up to "+str(n))
#print("Potential elements of covtree or cardinality up to "+str(k)+" by connectivity of common cover graph of rank "+str(n))
#print_covtree(CT)
print_covtree_with_witnesses(CT)

#print to a file too (from https://stackabuse.com/writing-to-a-file-with-pythons-print-function/)
import sys
original_stdout = sys.stdout # Save a reference to the original standard output
with open('covtree.txt', 'w') as f:
  sys.stdout = f # Change the standard output to the file we created.
  print("Elements of covtree of cardinality "+str(k)+ " and level " + str(l) + " and witnesses up to "+str(n))
  #print("Elements of covtree with witnesses of size up to "+str(n))
  #print("Elements of covtree with two posets and witnesses of size up to "+str(n))
  #print("Potential elements of covtree by connectivity of common cover graph")
  #print_covtree(CT)
  print_covtree_with_witnesses(CT)
  sys.stdout = original_stdout # Reset the standard output to its original value
