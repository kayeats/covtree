# how large the witnesses we will consider (largest considered is size n-1)
n = 8
#k = 3
#l = 4

# are two multisets of unlablled posets the same
# that is, are two lists the same up to order and isomorphism of the posets
def iso_multisets(S1, S2):
  if (len(S1) == 0) or (len(S2) == 0):
    return len(S1)==len(S2)
  i = S1[0]
  #find a copy of i in S2
  for j in S2:
    if i.is_isomorphic(j):
      S1new = S1.copy()
      S1new.remove(i)
      S2new = S2.copy()
      S2new.remove(j)        
      return iso_multisets(S1new, S2new)
  return False


# append if distinct, for the case of multisets of unlabelled posets
# L a list of multisets of posets, S a multiset of posets
# multisets are implemented as lists
def append_distinct(L, S, witness):
  found = False
  for [elt, witness2] in L:
    if iso_multisets(elt, S):
      found = True
  if not found:
    L.append([S, witness])

#print this behemoth
def print_multiset_covtree_with_witnesses(CT):
  #print("Elements of covtree with witnesses of size up to " + str(n-1))
  for i in range(len(CT)):
    print("Elements involving posets of size " + str(i))
    for [j,witness] in CT[i]:
      print("elt with " + str(len(j)) + " poset(s) of size " + str(i) + ": {")
      for k in j:
        print("["+str(k.list())+","+str(k.cover_relations())+"]")
      print("}")
      print("witness: "+str(witness.list())+","+str(witness.cover_relations()))

# build all elements of multiset covtree with witnesses up to size n - 1
# multisets are just lists.
# for now just collect them by the sizes of their elements, not building
# the poset structure of covtree itself.
def build_multiset_covtree(n):
  CT = [[] for i in range(n)]
  for witness_size in range(n):
    P = Posets(witness_size)
    for witness in P:
      print(".", end="", flush=True)
      level_sets = witness.order_ideals_lattice().level_sets()
      for size in range(len(level_sets)):
        append_distinct(CT[size], [witness.subposet(elts).canonical_label() for elts in level_sets[size]], witness)
    print("")
  return CT

##for building all of covtree up to a certain cover size
CT = build_multiset_covtree(n)

#print("Elements of multiset covtree of cardinality "+str(k)+ " and level " + str(l) + " and witnesses up to "+str(n))
#print_multiset_covtree_with_witnesses(CT)

for i in range(len(CT)):
  for [j, witness] in CT[i]:
    #print(len(j)+i-len(witness.list()))
    if len(j)+i-len(witness.list()) < 1:
      print("elt with " + str(len(j)) + " poset(s) of size " + str(i) + " and witness size " + str(len(witness.list())) + " which is BIG: {")
      for k in j:
        print("["+str(k.list())+","+str(k.cover_relations())+"]")
      print("}")
      print("witness: "+str(witness.list())+","+str(witness.cover_relations()))
  